const mapStyle = [
  {
    stylers: [{ visibility: "on" }],
  },
  {
    featureType: "landscape",
    elementType: "geometry",
    stylers: [{ visibility: "on" }, { color: "#fcfcfc" }],
  },
  {
    featureType: "water",
    elementType: "geometry",
    stylers: [{ visibility: "on" }, { color: "#bfd4ff" }],
  },
];

//Initialize and add the map
function initMap() {
var taiwan_center = {lat: 23.7374865, lng: 120.8647688};
map = new google.maps.Map(
//document.getElementById('map'), {zoom: 10, center: taiwan_center, mapTypeId: 'hybrid'});
//document.getElementById('map'), {zoom: 10, center: taiwan_center, mapTypeId: 'roadmap'});

document.getElementById('map'), {
	zoom: 8,
	center: taiwan_center,
	mapTypeId: 'roadmap',
	styles: mapStyle,});
var marker = new google.maps.Marker({position: taiwan_center, map: map});

populationMin = Number.MAX_VALUE;
populationMax = -Number.MAX_VALUE;
co2Min = Number.MAX_VALUE;
co2Max = -Number.MAX_VALUE;

  loadMapShapes();

//  loadPopulationData();

  map.data.addListener("mouseover", mouseInToRegion);
//  map.data.addListener("mouseout", mouseOutOfRegion);
  map.data.setStyle(styleFeature);
}

function mouseInToRegion(e) {
  const percent = ((e.feature.getProperty("population") - populationMin) / (populationMax - populationMin)) * 100;
  document.getElementById('data-city').textContent = e.feature.getProperty('city');
  document.getElementById('data-population').textContent = e.feature.getProperty('population').toLocaleString();
  //document.getElementById("controls").textContent = (e.feature.getProperty("population")).toLocaleString();
  //document.getElementById("data-box").style.display = "block";
  document.getElementById("data-caret").style.display = "block";
  document.getElementById("data-caret").style.paddingLeft = percent + "%";

}

function loadCo2Data() {
$.ajax({
 type: "GET",
  //url: "/get-dataset/mydata.json",
  url: "/get-dataset/co2_2015.json",
  success: function(data){
    let heatmapData = [];
    console.log("loadCo2Data success data = " + data);
    data['coordinates'].forEach(function (item, index) {
      //console.log(item, index);
      let locationData = {
	location: new google.maps.LatLng(item['latitude'],  item['longitude']),
	weight: item['weights']
      };
      heatmapData.push(locationData);
      let co2Value = item['properties']['xco2'];
      if (co2Value < co2Min) {
        co2Min = co2Value;
      }

      if (co2Value > co2Max) {
        co2Max = co2Value;
      } // update the existing row with the new data
    });

      // Update and display the max and min of CO2
      document.getElementById("co2-min").textContent = co2Min;
      document.getElementById("co2-max").textContent = co2Max;
  let heatmap = new google.maps.visualization.HeatmapLayer({
	  data: heatmapData,
	  dissipating: true,
	  radius: 20, // 半徑
	  opacity: 0.5,
	  gradient: ['rgba(255, 255, 255, 0)', // White
	              /*
		      //'red',
		      '#B3E5FC',
	              '#81D4FA',
	              '#4FC3F7',
	              '#29B6F6',
	              '#03A9F4',
	              '#039BE5',
	              '#0288D1',
	              '#0277BD',
	              '#01579B'], // 顏色
	              */
		      /*
		      '#B2FF59', // Light green A200
	              '#EEFF41', // Lime A200
	              '#FF0000', // yellow A200
	              '#FFD740', // Amber A200
	              '#FFC400', // Amber A400
	              '#FF9100', // Orange A400
	              '#FF3D00', // Deep orange A400
	              '#FF1744', // Red A400
	              '#D50000'],// Red A700 // 顏色
	              */
		      '#B3EFFC', // Light blue 100
	              '#81D4FA', // Light blue 200
	              '#64B5F6', // Blue 300
	              '#42A5F5', // Blur 400
	              '#3F51B5', // Indigo A500
	              '#3949AB', // Indigo A600
	              '#303F9F', // Indigo A700
	              '#4527A0', // Deep purple A800
	              '#311B92'],// Deep purple A900
		      /*
		      '#FAFAFA', // Grey 100
	              '#EEEEEE', // Grey 200
	              '#E0E0E0', // Grey 300
	              '#9E9E9E', // Grey 500
	              '#757575', // Grey 600
	              '#616161', // Grey 700
	              '#424242', // Grey 800
	              '#212121', // Grey 900
	              '#000000'],// Black
	              */
	  map: map
});
heatmap.setMap(map);
  },
  error: function(data){
    console.log("error data = " + data[0]);
  }
});
}


/** Loads the state boundary polygons from a GeoJSON source. */
function loadMapShapes() {
  // load US state outline polygons from a GeoJson file
//console.log("Load America json");
//map.data.loadGeoJson(
//  "https://storage.googleapis.com/mapsdevsite/json/states.js",
//  { idPropertyName: "STATE" }
//);

/*
map.data.loadGeoJson("/get-dataset/taiwan.json");
*/

/*
map.data.loadGeoJson(
  "/get-dataset/taiwan.json",
  { idPropertyName: "COUNTYID" }
);
*/

console.log("Load Taiwan json");
map.data.loadGeoJson('/get-dataset/taiwan.json', { idPropertyName: "COUNTYID" } , function(features) {
	/*
	  console.log("Logging the data features:");
	  console.log(features);
	  console.log("Using map.data.forEach:");
	    features.forEach(function(feature) {
	        console.log(feature);
		  });
	*/
	loadPopulationData();
		  })

  // wait for the request to complete by listening for the first feature to be
  // added
  /*
  google.maps.event.addListenerOnce(map.data, "addfeature", () => {
    google.maps.event.trigger(
      document.getElementById("census-variable"),
      "change"
    );
  });
  */

/*
	    console.log("features = ");
map.data.forEach(function(feature) {
	    console.log("feature = " + feature);
	});
	    console.log("loadPopulationData = ");
*/
}

function loadPopulationData() {
  /*
  // load the requested variable from the census API (using local copies)
  const xhr = new XMLHttpRequest();
  xhr.open("GET", variable + ".json");

  xhr.onload = function () {
  const censusData = JSON.parse(xhr.responseText);
  */
  $.ajax({
    type: "GET",
    url: "/get-dataset/population.json",
    async:false,
    success: function(data){
      let heatmapData = [];
      //console.log("loadPopulationData success data = ");
      //console.log(data);

      //censusData.shift(); // the first row contains column names
      //censusData.forEach((row) => {
      data['data'].forEach(function (item, index) {
        const population = parseInt(item["POPULATION"], 10);
        // const censusVariable = parseFloat(row[0]);
        const countyid = item["COUNTYID"]
        // const stateId = row[1]; // keep track of min and max values
        const city = item["CITY"];

        if (population < populationMin) {
          populationMin = population;
        }

        if (population > populationMax) {
          populationMax = population;
        } // update the existing row with the new data

	//console.log("populationMax = " + populationMax);
	//console.log("populationMin = " + populationMin);
	//console.log("population = " + population);
	//console.log("countyid = " + countyid);
	//console.log("city = " + city);

        map.data
          //.getFeatureById(stateId)
          .getFeatureById(countyid)
          //.setProperty("census_variable", censusVariable);
          .setProperty("population", population);

        map.data
          //.getFeatureById(stateId)
          .getFeatureById(countyid)
          //.setProperty("census_variable", censusVariable);
          .setProperty("city", city);
      });

      // Update and display the max and min of population
      document.getElementById("population-min").textContent = populationMin.toLocaleString();
      document.getElementById("population-max").textContent = populationMax.toLocaleString();
  },
  error: function(data){
    console.log("error data = " + data[0]);
    console.log("error data = " + data[1]);
  }
});//ajax end
  loadCo2Data();
}

function styleFeature(feature) {
	//console.log("feature.getProperty population = " + feature.getProperty("population"));
        const low = [5, 69, 54]; // color of smallest datum

        const high = [151, 83, 34]; // color of largest datum
        // delta represents where the value sits between the min and max

        const delta =
          //(feature.getProperty("census_variable") - censusMin) /
          (feature.getProperty("population") - populationMin) /
          (populationMax - populationMin);
        const color = [];

        for (let i = 0; i < 3; i++) {
          // calculate an integer color based on the delta
          color.push((high[i] - low[i]) * delta + low[i]);
        } // determine whether to show this shape or not

        let showRow = true;

        if (
          //feature.getProperty("census_variable") == null ||
          feature.getProperty("population") == null ||
          //isNaN(feature.getProperty("census_variable"))
          isNaN(feature.getProperty("population"))
        ) {
          showRow = false;
        }

        let outlineWeight = 0.5,
          zIndex = 1;

        if (feature.getProperty("state") === "hover") {
          outlineWeight = zIndex = 2;
        }

        return {
          strokeWeight: outlineWeight,
          strokeColor: "#fff",
          zIndex: zIndex,
          fillColor:
            "hsl(" + color[0] + "," + color[1] + "%," + color[2] + "%)",
          fillOpacity: 0.5,
	  visible: showRow,
        };
      }
