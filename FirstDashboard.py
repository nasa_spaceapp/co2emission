from flask import Flask, render_template, request, jsonify, send_from_directory
import plotly
import plotly.graph_objs as go

import pandas as pd
import numpy as np
import json

app = Flask(__name__)

app.config['NASA_DATASET'] = './dataset'
@app.route('/')
def index():
    feature = 'Bar'
    bar = create_plot(feature)
    return render_template('index.html', plot=bar)

def create_plot(feature):
    if feature == 'Bar':
        N = 30
        x = np.linspace(1, 30, N)
        x = reversed(x)
        y = np.random.randint(0,14,N)
        
        df = pd.DataFrame({'x': x, 'y': y}) # creating a sample dataframe
        data = [
            go.Bar(
                x=df['x'], # assign x as the dataframe column 'x'
                y=df['y']
            )
        ]
    else:
        N = 1000
        random_x = np.random.randn(N)
        random_y = np.random.randn(N)

        # Create a trace
        data = [go.Scatter(
            x = random_x,
            y = random_y,
            mode = 'markers'
        )]
    graphJSON = json.dumps(data, cls=plotly.utils.PlotlyJSONEncoder)

    return graphJSON

# This function will be periodly called by JavaScript in /static/js/auto-get.js
@app.route('/update', methods=['GET', 'POST'])
def updpate_num():
    example = {"patient_N":"123", "position_N":"456", "corr_position_N":"789", 
               "accuracy":"99.5", "unch_position_N":"135", "erro_position_N":"246"}
    #return JSON data to client
    return jsonify(example)

@app.route("/get-dataset/<path:path>")
def get_image(path):
    try:
        return send_from_directory(app.config["NASA_DATASET"], filename=path, as_attachment=False)
    except FileNotFoundError:
        abort(404)

@app.route('/bar', methods=['GET', 'POST'])
def change_features():
    feature = request.args['selected']
    graphJSON= create_plot(feature)
    
    return graphJSON

if __name__ == '__main__':
    from os import path, walk

    extra_dirs = ['templates/',]
    extra_files = extra_dirs[:]
    for extra_dir in extra_dirs:
        for dirname, dirs, files in walk(extra_dir):
            for filename in files:
                filename = path.join(dirname, filename)
                if path.isfile(filename):
                    extra_files.append(filename)
    
    app.run(host="0.0.0.0", extra_files=extra_files, debug=True, port=5999)
    
